﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListView 
{
    public const int MinPoolSize = 20;

    protected readonly int columnCount;
    protected float cellWidth;
    protected float cellHeight;

    public IListDataSource dataSource;
    protected int currentItemCount;

    protected GridLayoutGroup listViewFitter = null;
    protected readonly Vector3[] corners = new Vector3[4];
    protected RectTransform cellPrefab;
    protected RectTransform viewport;
    protected RectTransform content;
    protected Bounds viewBounds;
    protected int topMostCellIndex;
    protected int bottomMostCellIndex;
    protected int topMostCellColumn;
    protected int bottomMostCellColumn;

    protected List<RectTransform> cellPool;

    protected bool recycling = false;
    float itemOffsetHeight;
    float itemOffsetWidth;

    public ListView(RectTransform PrototypeCell, RectTransform Viewport, RectTransform Content, IListDataSource DataSource, int ColumnCount, GridLayoutGroup ListViewFitter)
    {
        cellPrefab = PrototypeCell;
        viewport = Viewport;
        content = Content;
        dataSource = DataSource;
        columnCount = ColumnCount;
        viewBounds = new Bounds();
        listViewFitter = ListViewFitter;
    }


    #region event handlers

    public IEnumerator InitCoroutine(System.Action OnInitialized = null)
    {
        SetAnchor(content);
        content.anchoredPosition = Vector3.zero;

        yield return null;
        SetBounds();

        CreatePool();
        currentItemCount = cellPool.Count;
        topMostCellIndex = 0;
        bottomMostCellIndex = cellPool.Count - 1;

        //Set content height according to no of rows
        int noOfRows = Mathf.CeilToInt((float)cellPool.Count / (float)columnCount);
        float contentYSize = noOfRows * (cellHeight + listViewFitter.spacing.y);
        content.sizeDelta = new Vector2(content.sizeDelta.x, contentYSize);
        SetAnchor(content);

        itemOffsetHeight = cellHeight + listViewFitter.spacing.y;
        itemOffsetWidth = cellWidth + listViewFitter.spacing.x;

        if (OnInitialized != null)
        {
            OnInitialized();
        }

        yield break;
    }

    public Vector2 OnValueChangedListener(Vector2 Direction)
    {
        if (recycling || cellPool == null || cellPool.Count == 0)
        {
            return Vector2.zero;
        }

        //Updating view bounds since it can change with resolution changes.
        SetBounds();

        var bottomMost = GetCorners(cellPool[bottomMostCellIndex]);
        if (Direction.y > 0 && bottomMost[1].y > viewBounds.min.y)
        {
            return RecycleTopToBottom();
        }

        var topMost = GetCorners(cellPool[topMostCellIndex]);
        if (Direction.y < 0 && topMost[0].y < viewBounds.max.y)
        {
            return RecycleBottomToTop();
        }

        return Vector2.zero;
    }

    #endregion


    #region internal methods

    private Vector2 RecycleBottomToTop()
    {
        // we don't want any games right now while we do this :D
        recycling = true;

        int newRowCount = 0;
        float posY = cellPool[topMostCellIndex].anchoredPosition.y;
        float posX = 0;

        //to determine if content size needs to be updated
        int additionalRows = 0;

        while (GetCorners(cellPool[bottomMostCellIndex])[1].y  < viewBounds.min.y && currentItemCount > cellPool.Count)
        {            
            if (--topMostCellColumn < 0)
            {
                ++newRowCount;
                topMostCellColumn = columnCount - 1;
                posY = cellPool[topMostCellIndex].anchoredPosition.y + itemOffsetHeight;
                ++additionalRows;
            }

            //Move bottom cell to top
            posX = topMostCellColumn * (itemOffsetWidth);
            cellPool[bottomMostCellIndex].anchoredPosition = new Vector2(posX, posY);

            if (--bottomMostCellColumn < 0)
            {
                bottomMostCellColumn = columnCount - 1;
                --additionalRows;
            }
            
            --currentItemCount;

            var cellPrefab = cellPool[bottomMostCellIndex];
            var thumb = cellPrefab.GetComponent<Thumbnail>();
            thumb.thumbnailVO = dataSource.GetCell(currentItemCount - cellPool.Count);

            //set new indices
            topMostCellIndex = bottomMostCellIndex;
            bottomMostCellIndex = (bottomMostCellIndex - 1 + cellPool.Count) % cellPool.Count;
        }

        //Content size adjustment 
        var itemOffsetVector = Vector2.up * itemOffsetHeight;
        if (columnCount > 1)
        {
            content.sizeDelta += additionalRows * itemOffsetVector;
            if (additionalRows > 0)
            {
                newRowCount -= additionalRows;
            }
        }

        cellPool.ForEach((RectTransform cell) => cell.anchoredPosition -= newRowCount * itemOffsetVector);
        content.anchoredPosition += newRowCount * itemOffsetVector;

        recycling = false;

        return new Vector2(0, newRowCount * itemOffsetHeight);
    }

    private Vector2 RecycleTopToBottom()
    {
        // we don't want any games right now while we do this :D
        recycling = true;

        int newRowCount = 0;
        float posY = cellPool[bottomMostCellIndex].anchoredPosition.y;
        float posX = 0;

        //to determine if content size needs to be updated
        int additionalRows = 0;

        while (GetCorners(cellPool[topMostCellIndex])[0].y  > viewBounds.max.y && currentItemCount < dataSource.GetItemCount())
        {            
            if (++bottomMostCellColumn >= columnCount)
            {
                ++newRowCount;
                bottomMostCellColumn = 0;
                posY = cellPool[bottomMostCellIndex].anchoredPosition.y - itemOffsetHeight;
                ++additionalRows;
            }

            //Move top cell to bottom
            posX = bottomMostCellColumn * (itemOffsetWidth);
            cellPool[topMostCellIndex].anchoredPosition = new Vector2(posX, posY);

            if (++topMostCellColumn >= columnCount)
            {
                topMostCellColumn = 0;
                --additionalRows;
            }

            var cellPrefab = cellPool[topMostCellIndex];
            var thumb = cellPrefab.GetComponent<Thumbnail>();
            thumb.thumbnailVO = dataSource.GetCell(currentItemCount);

            //set new indices
            bottomMostCellIndex = topMostCellIndex;
            topMostCellIndex = (topMostCellIndex + 1) % cellPool.Count;

            ++currentItemCount;
        }

        //Content size adjustment
        var itemOffsetVector = Vector2.up * itemOffsetHeight;
        if (columnCount > 1)
        {
            content.sizeDelta += additionalRows * itemOffsetVector;
            if (additionalRows > 0)
            {
                newRowCount -= additionalRows;
            }
        }

        //Content anchor position adjustment.
        cellPool.ForEach((RectTransform cell) => cell.anchoredPosition += newRowCount * itemOffsetVector);
        content.anchoredPosition -= newRowCount * itemOffsetVector;

        // reactivate events
        recycling = false;

        return -new Vector2(0, newRowCount * itemOffsetHeight);
    }

    private void ResetPool()
    {
        cellPool.ForEach((RectTransform item) => UnityEngine.Object.Destroy(item.gameObject));
        cellPool.Clear();
    }

    private void CreatePool()
    {
        Debug.Log("creating object pool...");

        if (cellPool != null)
        {
            ResetPool();
        }
        else
        {
            cellPool = new List<RectTransform>();
        }

        SetAnchorTopLeft(cellPrefab);
        
        topMostCellColumn = bottomMostCellColumn = 0;

        float currentPoolCoverage = 0;
        int poolSize = 0;
        float posX = 0;
        float posY = 0;

        //set new cell size according to its aspect ratio
        cellWidth = listViewFitter.cellSize.x ;
        cellHeight = listViewFitter.cellSize.y / listViewFitter.cellSize.x * cellWidth;

        //Get the required pool coverage and mininum size for the Cell pool
        float coverage = 1.25f * viewport.rect.height;
        int minPoolSize = Math.Min(MinPoolSize, dataSource.GetItemCount());

        //create cells untill the Pool area is covered and pool size is the minimum required
        while ((poolSize < minPoolSize || currentPoolCoverage < coverage) && poolSize < dataSource.GetItemCount())
        {
            RectTransform item = (UnityEngine.Object.Instantiate(cellPrefab.gameObject)).GetComponent<RectTransform>();

            posX = bottomMostCellColumn * (cellWidth + listViewFitter.spacing.x);

            item.name = "Cell";
            item.sizeDelta = new Vector2(cellWidth, cellHeight);
            item.SetParent(content, false);
            item.anchoredPosition = new Vector2(posX, posY);

            cellPool.Add(item);

            if (++bottomMostCellColumn >= columnCount)
            {
                bottomMostCellColumn = 0;
                posY -= (cellHeight + listViewFitter.spacing.y);
                currentPoolCoverage += (item.rect.height );
            }

            var thumb = item.GetComponent<Thumbnail>();
            thumb.thumbnailVO = dataSource.GetCell(poolSize);

            ++poolSize;
        }

        bottomMostCellColumn = (bottomMostCellColumn - 1 + columnCount) % columnCount;

        Debug.Log($"created object pool size {poolSize}");
    }

    private void SetBounds()
    {
        viewport.GetWorldCorners(corners);
        float threshHold = 0.2f * (corners[2].y - corners[0].y);
        viewBounds.min = new Vector3(corners[0].x, corners[0].y - threshHold);
        viewBounds.max = new Vector3(corners[2].x, corners[2].y + threshHold);
    }

    static Vector3[] GetCorners(RectTransform rectTransform)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);
        return corners;
    }

    private void SetAnchor(RectTransform RectTransform)
    {
        //Saving to reapply after anchoring. Width and height changes if anchoring is changed. 
        float width = RectTransform.rect.width;
        float height = RectTransform.rect.height;
        RectTransform.anchorMin = new Vector2(0.5f, 1);
        RectTransform.anchorMax = new Vector2(0.5f, 1);
        RectTransform.pivot = new Vector2(0.5f, 1);
        RectTransform.sizeDelta = new Vector2(width, height);
    }
    private void SetAnchorTopLeft(RectTransform RectTransform)
    {
        float width = RectTransform.rect.width;
        float height = RectTransform.rect.height;
        RectTransform.anchorMin = new Vector2(0, 1);
        RectTransform.anchorMax = new Vector2(0, 1);
        RectTransform.pivot = new Vector2(0, 1);
        RectTransform.sizeDelta = new Vector2(listViewFitter.cellSize.x, listViewFitter.cellSize.y);
    }

    #endregion
}
