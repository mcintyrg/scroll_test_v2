﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IListDataSource 
{
    int GetItemCount();
    ThumbnailVO GetCell(int index);
}
