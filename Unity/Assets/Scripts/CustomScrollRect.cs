﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Hook into the unity event we need by extending unity's scroll rect 
 */
 [System.Serializable]
public class CustomScrollRect : ScrollRect
{
    [SerializeField] public GameObject dataSource;
    [SerializeField] public RectTransform cellPrefab;

    private ListView listView;
    private Vector2 prevAnchoredPos;

    #region event handlers

    public void OnValueChangedListener(Vector2 normalizedPos)
    {
        Vector2 dir = content.anchoredPosition - prevAnchoredPos;
        m_ContentStartPosition += listView.OnValueChangedListener(dir);
        prevAnchoredPos = content.anchoredPosition;
    }

    #endregion

    #region unity hooks

    protected override void Start()
    {
        if (!Application.isPlaying)
        {
            return;
        }

        Initialize();

        StartCoroutine(listView.InitCoroutine(() => onValueChanged.AddListener(OnValueChangedListener)));
    }

    #endregion

    #region internal

    protected void Initialize()
    {
        if (cellPrefab == null)
        {
            Debug.LogError("must have a pool prefab");
            return;
        }
        if (dataSource == null)
        {
            Debug.LogError("must have a data source");
            return;
        }

        var source = (IListDataSource) dataSource.GetComponent(typeof(IListDataSource));

        var listViewFitter = GetComponentInChildren<GridLayoutGroup>();

        listView = new ListView(cellPrefab, viewport, content, source, 5, listViewFitter);

        prevAnchoredPos = content.anchoredPosition;

    }

    #endregion
}
