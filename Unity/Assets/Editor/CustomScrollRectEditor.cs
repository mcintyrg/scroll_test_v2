﻿using UnityEditor;
using UnityEngine.UI;
using UnityEditor.AnimatedValues;

[CustomEditor(typeof(CustomScrollRect), true)]
[CanEditMultipleObjects]
public class CustomScrollRectEditor : Editor
{
    protected SerializedProperty content;
    protected SerializedProperty movementType;
    protected SerializedProperty elasticity;
    protected SerializedProperty inertia;
    protected SerializedProperty decelerationRate;
    protected SerializedProperty scrollSensitivity;
    protected SerializedProperty viewport;
    protected SerializedProperty onValueChanged;
    protected AnimBool showElasticity;
    protected AnimBool showDecelerationRate;

    protected SerializedProperty cellPrefab;
    protected SerializedProperty dataSource;

    protected virtual void OnEnable()
    {
        content = serializedObject.FindProperty("m_Content");
        movementType = serializedObject.FindProperty("m_MovementType");
        elasticity = serializedObject.FindProperty("m_Elasticity");
        inertia = serializedObject.FindProperty("m_Inertia");
        decelerationRate = serializedObject.FindProperty("m_DecelerationRate");
        scrollSensitivity = serializedObject.FindProperty("m_ScrollSensitivity");
        viewport = serializedObject.FindProperty("m_Viewport");
        onValueChanged = serializedObject.FindProperty("m_OnValueChanged");
        showElasticity = new AnimBool(Repaint);
        showDecelerationRate = new AnimBool(Repaint);
        SetAnimBools(true);

        cellPrefab = serializedObject.FindProperty("cellPrefab");
        dataSource = serializedObject.FindProperty("dataSource");
    }

    protected virtual void OnDisable()
    {
        showElasticity.valueChanged.RemoveListener(Repaint);
        showDecelerationRate.valueChanged.RemoveListener(Repaint);
    }

    void SetAnimBools(bool instant)
    {
        SetAnimBool(showElasticity, !movementType.hasMultipleDifferentValues && movementType.enumValueIndex == (int)ScrollRect.MovementType.Elastic, instant);
        SetAnimBool(showDecelerationRate, !inertia.hasMultipleDifferentValues && inertia.boolValue, instant);
    }

    void SetAnimBool(AnimBool a, bool value, bool instant)
    {
        if (instant)
            a.value = value;
        else
            a.target = value;
    }

    public override void OnInspectorGUI()
    {
        SetAnimBools(false);
        serializedObject.Update();

        EditorGUILayout.PropertyField(viewport);
        EditorGUILayout.PropertyField(content);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(movementType);
        if (EditorGUILayout.BeginFadeGroup(showElasticity.faded))
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(elasticity);
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFadeGroup();

        EditorGUILayout.PropertyField(inertia);
        if (EditorGUILayout.BeginFadeGroup(showDecelerationRate.faded))
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(decelerationRate);
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFadeGroup();

        EditorGUILayout.PropertyField(scrollSensitivity);

        // extra fields from the noral editor
        EditorGUILayout.PropertyField(cellPrefab);
        EditorGUILayout.PropertyField(dataSource);


        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(onValueChanged);

        serializedObject.ApplyModifiedProperties();
    }
}
